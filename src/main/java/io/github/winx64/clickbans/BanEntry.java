package io.github.winx64.clickbans;

import java.util.LinkedHashMap;
import java.util.Map;

import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;

import io.github.winx64.clickbans.util.Util;

@SerializableAs("BanEntry")
public final class BanEntry implements ConfigurationSerializable {

	private static final String NAME_KEY = "name";
	private static final String REASON_KEY = "reason";
	private static final String BANNED_BY_KEY = "banned-by";
	private static final String DATE_KEY = "date";

	private final String name;
	private final String reason;
	private final String bannedBy;
	private final int date;

	public BanEntry(String name, String reason, String bannedBy) {
		this.name = name;
		this.reason = reason;
		this.bannedBy = bannedBy;
		this.date = Util.unixTimestamp();
	}

	public BanEntry(Map<String, Object> map) {
		Object nameObject = map.get(NAME_KEY);
		if (nameObject == null || !(nameObject instanceof String)) {
			throw new IllegalArgumentException("name cannot be null and must be a String");
		}

		Object reasonObject = map.get(REASON_KEY);
		if (reasonObject == null || !(reasonObject instanceof String)) {
			throw new IllegalArgumentException("reason cannot be null and must be a String");
		}

		Object bannedByObject = map.get(BANNED_BY_KEY);
		if (bannedByObject == null || !(bannedByObject instanceof String)) {
			throw new IllegalArgumentException("banned-by cannot be null and must be a String");
		}

		Object dateObject = map.get(DATE_KEY);
		if (dateObject == null || !(dateObject instanceof Number)) {
			throw new IllegalArgumentException("date cannot be null and must be an integer");
		}

		this.name = (String) nameObject;
		this.reason = (String) reasonObject;
		this.bannedBy = (String) bannedByObject;
		this.date = ((Number) dateObject).intValue();
	}

	public Map<String, Object> serialize() {
		Map<String, Object> map = new LinkedHashMap<String, Object>();

		map.put(NAME_KEY, name);
		map.put(REASON_KEY, reason);
		map.put(BANNED_BY_KEY, bannedBy);
		map.put(DATE_KEY, date);

		return map;
	}

	public String getName() {
		return name;
	}

	public String getReason() {
		return reason;
	}

	public String getBannedBy() {
		return bannedBy;
	}

	public int getDate() {
		return date;
	}

	public static BanEntry deserialize(Map<String, Object> map) {
		return new BanEntry(map);
	}
}
