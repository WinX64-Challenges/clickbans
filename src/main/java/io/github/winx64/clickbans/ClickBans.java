package io.github.winx64.clickbans;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.plugin.java.JavaPlugin;

import io.github.winx64.clickbans.command.CommandBan;
import io.github.winx64.clickbans.command.CommandUnban;
import io.github.winx64.clickbans.listener.PlayerListener;

public final class ClickBans extends JavaPlugin {

	private static final String BANS_KEY = "bans";
	private static final String BANS_FILE_NAME = "bans.yml";
	private static final String INVALID_BAN_OBJECT = "Invalid ban entry of type '%s'!";

	private static final String BANS_LOADED = "%d bans loaded!";
	private static final String BANS_SAVED = "%d bans saved!";

	private final File bansFile;
	private final Map<String, BanEntry> bans;

	public ClickBans() {
		this.bansFile = new File(getDataFolder(), BANS_FILE_NAME);
		this.bans = new HashMap<String, BanEntry>();
	}

	@Override
	public void onEnable() {
		ConfigurationSerialization.registerClass(BanEntry.class);

		Bukkit.getPluginManager().registerEvents(new PlayerListener(this), this);
		getCommand("ban").setExecutor(new CommandBan(this));
		getCommand("unban").setExecutor(new CommandUnban(this));

		if (!bansFile.exists()) {
			this.saveResource(BANS_FILE_NAME, true);
		}

		loadBans();
		getLogger().info(String.format(BANS_LOADED, bans.size()));
	}

	@Override
	public void onDisable() {
		saveBans();
		getLogger().info(String.format(BANS_SAVED, bans.size()));
	}

	public BanEntry getBanEntry(String name) {
		return bans.get(name.toLowerCase());
	}

	public Set<String> getBannedPlayers() {
		return Collections.unmodifiableSet(bans.keySet());
	}

	public void ban(String name, String reason, String bannedBy) {
		this.bans.put(name.toLowerCase(), new BanEntry(name, reason, bannedBy));
	}

	public void unban(String name) {
		this.bans.remove(name.toLowerCase());
	}

	private void loadBans() {
		try {
			YamlConfiguration bansConfig = YamlConfiguration.loadConfiguration(bansFile);
			List<?> banList = bansConfig.getList(BANS_KEY);

			for (Object banObject : banList) {
				if (banObject instanceof BanEntry) {
					BanEntry banEntry = (BanEntry) banObject;
					this.bans.put(banEntry.getName().toLowerCase(), banEntry);
				} else {
					getLogger().warning(String.format(INVALID_BAN_OBJECT, banObject.getClass().getSimpleName()));
				}
			}
		} catch (Exception e) {
			getLogger().log(Level.SEVERE, "An error occurred while trying to load the bans! Details below:", e);
		}
	}

	private void saveBans() {
		try {
			YamlConfiguration bansConfig = new YamlConfiguration();
			bansConfig.set(BANS_KEY, new ArrayList<BanEntry>(bans.values()));
			bansConfig.save(bansFile);
		} catch (Exception e) {
			getLogger().log(Level.SEVERE, "An error occurred while trying to save the bans! Details below:", e);
		}
	}
}
