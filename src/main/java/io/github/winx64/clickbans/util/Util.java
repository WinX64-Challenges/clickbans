/*
 *   MaintenanceMode - Enable maintenance mode on your server
 *   Copyright (C) WinX64 2017
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.github.winx64.clickbans.util;

import java.util.concurrent.TimeUnit;

public final class Util {

	private Util() {}

	public static String stringJoin(String[] args, String joint) {
		StringBuilder builder = new StringBuilder();
		for (String arg : args) {
			builder.append(joint).append(arg);
		}
		return builder.length() < 1 ? "" : builder.substring(joint.length());
	}

	public static String formatCompleteTimeString(int totalSeconds) {
		StringBuilder builder = new StringBuilder();
		for (CompleteTimeUnit compUnit : CompleteTimeUnit.values()) {
			long unitTime = compUnit.unit.convert(totalSeconds, TimeUnit.SECONDS);
			if (unitTime > 0) {
				builder.append(unitTime).append(' ').append(unitTime == 1 ? compUnit.singular : compUnit.plural)
						.append(' ');
			}
			totalSeconds -= TimeUnit.SECONDS.convert(unitTime, compUnit.unit);
		}
		return builder.toString().trim();
	}

	public static int unixTimestamp() {
		return (int) (System.currentTimeMillis() / 1000);
	}

	private static enum CompleteTimeUnit {
		HOURS(TimeUnit.HOURS, "horas", "hora"),
		MINUTES(TimeUnit.MINUTES, "minutos", "minuto"),
		SECONDS(TimeUnit.SECONDS, "segundos", "segundo");

		private final TimeUnit unit;
		private final String plural;
		private final String singular;

		private CompleteTimeUnit(TimeUnit unit, String plural, String singular) {
			this.unit = unit;
			this.plural = plural;
			this.singular = singular;
		}
	}
}
