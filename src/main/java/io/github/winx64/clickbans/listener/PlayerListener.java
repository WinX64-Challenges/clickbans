package io.github.winx64.clickbans.listener;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;

import io.github.winx64.clickbans.BanEntry;
import io.github.winx64.clickbans.ClickBans;
import io.github.winx64.clickbans.util.Util;

public final class PlayerListener implements Listener {

	private static final String YOU_ARE_BANNED = ChatColor.RED + "" + ChatColor.BOLD
			+ "Você está banido desse servidor!" + ChatColor.RESET + "\n" + ChatColor.RED + "Por: " + ChatColor.RESET
			+ "%s\n" + ChatColor.RED + "Motivo: " + ChatColor.RESET + "%s\n" + ChatColor.RED + "Quando: "
			+ ChatColor.RESET + "%s atrás";

	private final ClickBans plugin;

	public PlayerListener(ClickBans plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onPlayerLogin(PlayerLoginEvent event) {
		BanEntry banEntry = plugin.getBanEntry(event.getPlayer().getName());
		if (banEntry != null) {
			int totalSeconds = Util.unixTimestamp() - banEntry.getDate();
			event.disallow(Result.KICK_BANNED, String.format(YOU_ARE_BANNED, banEntry.getBannedBy(),
					banEntry.getReason(), Util.formatCompleteTimeString(totalSeconds)));
		}
	}
}
