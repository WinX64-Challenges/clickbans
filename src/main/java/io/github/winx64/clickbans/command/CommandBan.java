package io.github.winx64.clickbans.command;

import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import io.github.winx64.clickbans.ClickBans;
import io.github.winx64.clickbans.util.Message;
import io.github.winx64.clickbans.util.Util;

public final class CommandBan implements CommandExecutor {

	private static final String SYNTAX = ChatColor.RED + "Sintaxe: /%s <jogador> [motivo...]";

	private static final String SELECT_REASON = ChatColor.RED + "Selecione um motivo para o banimento:";
	private static final String REASON = ChatColor.GRAY + " - %s";
	private static final String COMMAND_TIP = "/%s %s %s";

	private static final String CANNOT_SELF_BAN = ChatColor.RED + "Você não pode banir a si mesmo!";
	private static final String PLAYER_ALREADY_BANNED = ChatColor.RED + "O jogador especificado ja esta banido!";
	private static final String YOU_HAVE_BEEN_BANNED = ChatColor.RED + "" + ChatColor.BOLD
			+ "Você foi banido do servidor!" + ChatColor.RESET + "\n" + ChatColor.RED + "Por: " + ChatColor.RESET
			+ "%s\n" + ChatColor.RED + "Motivo: " + ChatColor.RESET + "%s";
	private static final String PLAYER_HAS_BEEN_BANNED = ChatColor.GREEN + "O jogador " + ChatColor.RESET + "%s "
			+ ChatColor.GREEN + "foi banido!";

	private static final List<String> REASONS = Arrays.asList("Forcefield", "Fly", "Speed", "NoKnockback", "Regen");

	private final ClickBans plugin;

	public CommandBan(ClickBans plugin) {
		this.plugin = plugin;
	}

	public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
		if (args.length < 1) {
			sender.sendMessage(String.format(SYNTAX, alias));
			return true;
		} else if (args.length < 2) {
			if (sender instanceof Player) {
				Player player = (Player) sender;
				player.sendMessage(SELECT_REASON);
				for (String reason : REASONS) {
					String commandTip = String.format(COMMAND_TIP, alias, args[0], reason);
					player.spigot().sendMessage(new Message(String.format(REASON, reason)).hover(commandTip)
							.clickCommand(commandTip).create());
				}
			} else {
				sender.sendMessage(String.format(SYNTAX, alias));
			}
			return true;
		}

		if (sender.getName().equalsIgnoreCase(args[0])) {
			sender.sendMessage(CANNOT_SELF_BAN);
			return true;
		}

		if (plugin.getBanEntry(args[0]) != null) {
			sender.sendMessage(PLAYER_ALREADY_BANNED);
			return true;
		}

		String reason = Util.stringJoin(Arrays.copyOfRange(args, 1, args.length), " ");
		plugin.ban(args[0], reason, sender.getName());

		sender.sendMessage(String.format(PLAYER_HAS_BEEN_BANNED, args[0]));
		Player target = Bukkit.getPlayerExact(args[0]);
		if (target != null) {
			target.kickPlayer(String.format(YOU_HAVE_BEEN_BANNED, sender.getName(), reason));
		}

		return true;
	}
}
