package io.github.winx64.clickbans.command;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;

import io.github.winx64.clickbans.ClickBans;

public final class CommandUnban implements TabExecutor {

	private static final String SYNTAX = ChatColor.RED + "Sintaxe: /%s <jogador> [motivo...]";

	private static final String PLAYER_NOT_BANNED = ChatColor.RED + "O jogador especificado não esta banido!";
	private static final String PLAYER_HAS_BEEN_UNBANNED = ChatColor.GREEN + "O jogador " + ChatColor.RESET + "%s "
			+ ChatColor.GREEN + "foi desbanido!";

	private static final List<String> EMPTY = Collections.emptyList();

	private final ClickBans plugin;

	public CommandUnban(ClickBans plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
		if (args.length < 1) {
			sender.sendMessage(String.format(SYNTAX, alias));
			return true;
		}

		if (plugin.getBanEntry(args[0]) == null) {
			sender.sendMessage(PLAYER_NOT_BANNED);
			return true;
		}

		plugin.unban(args[0]);
		sender.sendMessage(String.format(PLAYER_HAS_BEEN_UNBANNED, args[0]));
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		if (args.length == 1) {
			List<String> matches = new ArrayList<String>();
			String match = args[0].toLowerCase();
			for (String name : plugin.getBannedPlayers()) {
				if (name.startsWith(match)) {
					matches.add(name);
				}
			}

			return matches;
		}
		return EMPTY;
	}
}
